from time import sleep
import pygame as pg
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import signal


class PyGameTest:
    
    # Scale the image every N frames
    __scale_counter_tick:int = 15
    # Scale increments
    __scale_x:float = 0.01
    __scale_y:float = 0.02
    # Max FPS
    __fps:int = 60
    # Smoothscale Mode ("GENERIC", "SSE", "MMX", None)
    __scale_mode:str = "MMX"
    # Screen Size
    __screen_resolution:tuple[2] = (1024, 768)
    
    
    __screen:pg.surface.Surface
    __clock:pg.time.Clock
    __shutdown:bool = False
    __bg_surface:pg.surface.Surface
    __original_image:pg.surface.Surface
    __image:pg.surface.Surface

    def __init__(self,) -> None:
        
        screenFlags = pg.SCALED | pg.NOFRAME #| pg.OPENGL
        self.__screen = pg.display.set_mode(
                size=self.__screen_resolution,
                flags=screenFlags,display=0,vsync=False
        )
        
        if self.__scale_mode is not None:
            pg.transform.set_smoothscale_backend(self.__scale_mode)
        
        self.__scale_mode = pg.transform.get_smoothscale_backend()
        print("Utilizing",self.__scale_mode,"for smoothscale")
        
        pg.display.set_caption("PyGame Scale Test")
        
        while not pg.display.get_init():
            sleep(0.001)
        
        pg.mouse.set_visible(True)
        pg.event.set_grab(False)
        # pg.key.set_repeat(800,500)
        pg.key.set_repeat(0,0)
        
        self.__clock = pg.time.Clock()
        
        self.__bg_surface = pg.surface.Surface(self.__screen.get_size())
        self.__bg_surface.fill((0,0,0))
        
        self.__image = pg.image.load("test_image.png")
        self.__image.convert_alpha()
        self.__original_image = self.__image.copy()
        
            
    def run(self):
        
        image_x = (self.__screen.get_width() + 5) * 0.175
        image_y = self.__screen.get_height() * 0.05
        image_position = (image_x, image_y)
        self.__original_problem_image = self.__original_image.copy()
        self.__original_problem_image = pg.transform.smoothscale(
            self.__original_problem_image,
            (self.__screen.get_width() * 0.65, self.__screen.get_height() * 0.05)
        )
        
        x = 1.0
        y = 1.0
        __scale_counter = 0
        __reverse_scale = True
        while not self.__shutdown:
            
            __scale_counter += 1
            if __scale_counter >= self.__scale_counter_tick:
                __scale_counter = 0
                if not __reverse_scale:
                    x += self.__scale_x
                    y += self.__scale_y
                else:
                    x -= self.__scale_x
                    y -= self.__scale_y
                
                if (x >= 2 or x <= 0):
                    __reverse_scale = (not __reverse_scale)
                    x = 1.0
                    y = 1.0
                    
                self.__image = self.__original_image.copy()
                scale_size = (
                    self.__original_image.get_width() * x,
                    self.__original_image.get_height() * y
                )
                if scale_size[0] < 0 or scale_size[1] < 0:
                    __reverse_scale = (not __reverse_scale)
                    x = 1.0
                    y = 1.0
                    
                print("Scaling Image",scale_size,"Current Step",str(x))
                self.__image = pg.transform.smoothscale(self.__image,scale_size)
                
                
            
            pg.event.pump()
            
            self.__screen.blit(self.__bg_surface,(0,0))
            
            self.__screen.blit(self.__original_problem_image,image_position)
            
            self.__screen.blit(
                self.__image,
                (
                    (self.__screen.get_width() / 2) - (self.__image.get_width() / 2),
                    (self.__screen.get_height() / 2) - (self.__image.get_height() / 2)
                )
            )
            
            pg.display.update()
            pg.display.flip()
            self.__clock.tick(self.__fps)
        
    def halt(self, arg_a, arg_b):
        self.__shutdown = True

test = PyGameTest()

cap_signals = [ signal.SIGABRT, signal.SIGILL, signal.SIGINT, signal.SIGTERM ]
for s in cap_signals:
    signal.signal(s,test.halt)

test.run()